import axios from 'axios'
import consts from '../consts'
import { clear } from '../todo/todoActions'
import { toastr } from 'react-redux-toastr'

export function login(values){
    return submit(values, `${consts.OAPI_URL}/login`)
}

export function signup(values) {
    return submit(values, `${consts.OAPI_URL}/signup`)
}

export function logout() {
    return {type: 'TOKEN_VALIDATED', payload: false}
}

export function validateToken(token) {
    return dispatch => {
        if(token) {
            axios.post(`${consts.OAPI_URL}/validateToken`, {token}).then(resp => {
                dispatch({type: 'TOKEN_VALIDATED', payload: resp.data.valid})
            }).catch(e => dispatch({type: 'TOKEN_VALIDATED', payload: false}))
        } else {
            dispatch({type: 'TOKEN_VALIDATED', payload: false})
        }
    }
}

export function updatePreferredWorkingHourPerDay(preferredWorkingHourPerDay) {
    return (dispatch, getState) => {
        const { auth, id } = getState()
        if(auth.user.token) {
            axios.post(`${consts.API_URL}/updatePreferredWorkingHourPerDay?token=${auth.user.token}`, {...preferredWorkingHourPerDay, id: auth.user.id})
                .then(resp => {dispatch({type: 'USER_UPDATE_PREFERRED_WORKING', payload: resp.data})})
                .then(resp => dispatch(clear()))
                .catch(e => {
                    e.response.data.errors.forEach(
                        error => toastr.error('Ops!', error))
                })
        } else {
            dispatch({type: 'USER_UPDATE_PREFERRED_WORKING', payload: null})
        }
    }
}

function submit(values, url) {
    return dispatch => {
        axios.post(url, values).then(resp => {
            dispatch([{type: 'USER_FETCHED', payload: resp.data}])
        }).catch(e => {
                console.log(e)
                e.response.data.errors.forEach(
                    error => toastr.error('Ops!', error))
            })
    }
}