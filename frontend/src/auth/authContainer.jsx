import './auth.css'
import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { login, signup } from './authActions'
import If from '../template/if'
import Input from '../template/input'
import Select from '../template/select'
import LEVELS from '../levels'
import Messages from '../template/messages' 

class AuthContainer extends Component {
    constructor(props) {
        super(props)
        this.state = { loginMode: true }
    }

    changeMode() {
        this.setState({ loginMode: !this.state.loginMode })
    }

    onSubmit(values) {
        const { login, signup } = this.props
        this.state.loginMode ? login(values) : signup(values)
    }

    render(){
        const { loginMode } = this.state
        const { handleSubmit } = this.props
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 col-md-4 col-md-offset-4">
                        <div className="account-wall">
                            <h2>    
                                <a className='text-center new-account'>
                                    <i className='fa fa-calendar-check-o'></i> TodoApp
                                </a>
                            </h2>
                            <form onSubmit={handleSubmit(v => this.onSubmit(v))} className="form-signin">
                                <Field component={Input} type="input" name="name" placeholder="Name" icon='user' hide={loginMode} />
                                <Field component={Input} type="email" name="email" placeholder="E-mail" icon='envelope' />
                                <If test={!loginMode}>
                                    <Field component={Select} name="level" options={LEVELS} />
                                </If>
                                <Field component={Input} type="password" name="password" placeholder="Password" icon='lock' />
                                <Field component={Input} type="password" name="confirm_password" placeholder="Confirm your password" icon='lock' hide={loginMode} />

                                <button type="submit"
                                    className="btn btn-primary btn-block btn-flat">
                                    {loginMode ? 'Enter' : 'Register'}
                                </button>
                                
                            </form>
                            <br />
                            <a className="text-center new-account" 
                                onClick={() => this.changeMode()}> 
                                    {loginMode ? 'New User? Register now!' : 'Its already registered? Go!'}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

AuthContainer = reduxForm({ form: 'authForm' })(AuthContainer)
const mapDispatchToProps = dispatch => bindActionCreators({ login, signup }, dispatch)
export default connect(null, mapDispatchToProps)(AuthContainer)