import consts from '../consts'
const INITIAL_STATE = {
    user: JSON.parse(localStorage.getItem(consts.userKey)),
    validToken: false
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case 'TOKEN_VALIDATED': 
            if(action.payload) {
                return {...state, validToken: true}
            } else {
                localStorage.removeItem(consts.userKey)
                return { ...state, validToken: false, user: null}
            }
        case 'USER_FETCHED':
            localStorage.setItem(consts.userKey, JSON.stringify(action.payload))
            return { ...state, user: action.payload, validToken: true}
        case 'USER_UPDATE_PREFERRED_WORKING':
            if(action.payload){
                state.user.preferredWorkingHourPerDay = action.payload.preferredWorkingHourPerDay
                return state
            }
            return state
        default:
            return state
    }
}