import React from 'react'
import PageHeader from '../template/pageHeader'
import Menu from '../template/menu'

export default props => (
    <div>
        <Menu />
        <PageHeader name='About' small='Us'></PageHeader>
        <h2>History</h2>
        <p>Lorem ipsum dolor sit amet...</p>
    </div>
)