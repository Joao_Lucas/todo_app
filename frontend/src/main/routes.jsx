import React from 'react'
import { Router, Route, Redirect, hashHistory } from 'react-router'

import About from '../about/about'
import AuthOrApp from './authOrApp'
import Auth from '../auth/auth'
import User from '../user/user'

export default props => (
    <Router history={hashHistory}>
        <Route path='/' component={AuthOrApp} />
        <Route path='/about' component={About} />
        <Route path='/users' component={User} />
        <Redirect from='*' to='/' />
    </Router>
)