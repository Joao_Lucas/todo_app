import 'modules/bootstrap/dist/css/bootstrap.min.css'
import 'modules/font-awesome/css/font-awesome.min.css'
import '../template/custom.css'
import Messages from '../template/messages' 

import React from 'react'
import Routes from './routes'

export default props => (
    <div className='wrapper'>
        <div className='container'>
            <Routes />
        </div>
        <Messages />
    </div>
)