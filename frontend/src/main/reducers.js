import { combineReducers } from 'redux'
import todoReducer from '../todo/todoReducer'
import authReducer from '../auth/authReducer'
import AuthOrApp from './authOrApp'
import UserReducer from '../user/userReducer'

import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

const rootReducer = combineReducers({
    todo: todoReducer,
    auth: authReducer,
    form: formReducer,
    user: UserReducer,
    toastr: toastrReducer
})

export default rootReducer