import axios from 'axios'
import { change } from 'redux-form'
import LEVEL from '../levels'
import { toastr } from 'react-redux-toastr'

const URL = 'http://localhost:3003/api/todos'

export const changeDescription = event => ({
    type: 'DESCRIPTION_CHANGED',
    payload: event.target.value
})

export const search = (values = {}) => {
    return (dispatch, getState) => {
        const { auth } = getState();
        let search = auth.user.level == LEVEL.REGULAR ? `&userId=${auth.user.id}` : ''
        search = values.begin ? `${search}&workedAt__gte=${values.begin}` : search
        search = values.end ? `${search}&workedAt__lte=${values.end}T23:59:59.734Z` : search
        search = `${search}&token=${auth.user.token}`
        const request = axios.get(`${URL}?sort=-workedAt${search}`)
            .then(resp => dispatch({type: 'TODO_SEARCHED', payload: resp.data}))
            .catch(e => {
                console.log(e)
                toastr.error('Ops!', e.response.data.message)
            })
    }
}

export const add = (value) => {
    return (dispatch, getState) => {
        const { auth } = getState();
        value.userId = auth.user.id
        axios.post(`${URL}?token=${auth.user.token}`, value)
            .then(resp => dispatch(clear()))
            .then(resp => dispatch(search()))
            .catch(e => {
                toastr.error('Ops!', e.response.data.message)
            })
    }
}

export const edit = (id, todo) => {
    return (dispatch, getState) => {
        const { auth } = getState();
        axios.put(`${URL}/${id}?token=${auth.user.token}`, todo)
            .then(resp => dispatch(clear()))
            .then(resp => dispatch(search()))
            .catch(e => {
                toastr.error('Ops!', e.response.data.message)
            })
    }
}

export const remove = (todo) => {
    return (dispatch, getState) => {
        const { auth } = getState();
        axios.delete(`${URL}/${todo._id}?token=${auth.user.token}`)
            .then(resp => dispatch(search()))
            .catch(e => {
                toastr.error('Ops!', e.response.data.message)
            })
    }
}

export const clear = () => {
    return [{ type: 'TODO_CLEAR' }, search()]
}

