import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, change } from 'redux-form'

import Grid from '../template/grid'
import IconButton from '../template/iconButton'
import { updatePreferredWorkingHourPerDay } from '../auth/authActions'
import Input from '../template/input'

class TodoPreferredWorkingHourPerDayForm extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount(){
        this.props.dispatch(change( "todo-preferred-working-hour-day-form", "preferredWorkingHourPerDay", this.props.auth.user.preferredWorkingHourPerDay))
    }

    render() {
        const { updatePreferredWorkingHourPerDay } = this.props
        const { handleSubmit } = this.props
        return (
            <form onSubmit={handleSubmit(v => updatePreferredWorkingHourPerDay(v))} name="todo-preferred-working-hour-day-form">
                <div role='form' className='todoForm'>
                    <Grid cols='12 6 6'>
                        <Field component={Input} type="number" name="preferredWorkingHourPerDay" title="Add preferred working hour per day" placeholder="Add preferred working hour per day" />
                    </Grid>
                    <Grid cols='12 3 2'>
                        <IconButton style='primary' icon='hourglass-half' title='Set preferred working hour per day' type='submit'></IconButton>
                    </Grid>
                </div>
            </form>
        )
    }
}

TodoPreferredWorkingHourPerDayForm = reduxForm({ form: 'todo-preferred-working-hour-day-form' })(TodoPreferredWorkingHourPerDayForm)
const mapStateToProps = state => ({auth: state.auth})
const mapDispatchToProps = dispatch => bindActionCreators({ updatePreferredWorkingHourPerDay }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(TodoPreferredWorkingHourPerDayForm)