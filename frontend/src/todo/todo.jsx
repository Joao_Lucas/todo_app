import React from 'react'

import PageHeader from '../template/pageHeader'
import TodoSearch from './todoSearch'
import TodoForm from './todoForm'
import TodoList from './todoList'
import TodoPreferredWorkingHourPerDayForm from './todoPreferredWorkingHourPerDayForm'
import Menu from '../template/menu'

export default props => (
    <div>
        <Menu />
        <PageHeader name='TO DO List'></PageHeader>
        <TodoSearch />
        <TodoForm />
        <TodoPreferredWorkingHourPerDayForm />
        <TodoList />
    </div>
)