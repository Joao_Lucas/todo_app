import React,{ Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import IconButton from '../template/iconButton'
import { edit, remove, exportData } from './todoActions'
import { change } from 'redux-form'

class TodoList extends Component {
    constructor(props) {
        super(props)
        this.formatDate = this.formatDate.bind(this)
        this.exportData = this.exportData.bind(this)
    }

    exportData() {
        const list = this.props.list || []
        const map = this.createDataToExport(list)
        let html = ''
        html = html.concat(
            `<!DOCTYPE html>
            <html>
            <header>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            </header>
            <body>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <h1 class="text-success text-center">
                                TODOS Exported.
                            </h1>
                            ${this.createListTodos(map)}
                        </div>
                    </div>
                </div>
            </body>
            </html>`)
        let element = document.createElement("a");
        let file = new Blob([html], {type: 'html/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "exported_todos.html";
        element.click();
    }

    createListTodos(map){
        let html = '';
        for (const item of map.entries()){         
            html = html.concat(
                `<div class="list-group">
                    <a class="list-group-item active">Date: ${this.formatDate(item[1].workedAt)}</a>
                    <a class="list-group-item active">Total time: <span class="badge">${item[1].totalHours}</span></a>
                    ${this.createListNotes(item[1].notes)}
                </div>`
            )
        }
        return html
    }

    createListNotes(notes){
        let html = ''
        for (const note in notes){
            html = html.concat(
                `<div class="list-group-item">
                    ${notes[note]}
                </div>`
            )
        }
        return html
    }

    createDataToExport(list){
        const map = new Map()
        if( list.length > 0 ){
            for(let todo of list){
                const item = map.get(key)
                if(!item){
                    console.log('Não tem: ' + todo.workedAt)
                    map.set(key, {workedAt: todo.workedAt, totalHours: todo.howLong, notes: [todo.description]})
                } else {
                    item.totalHours = item.totalHours + todo.howLong
                    item.notes.push(todo.description)
                }
            }
        }
        return map
    }

    formatDate(date) {
        if(date){
            let parts = date.split('T')[0].split('-')
            let newDate = new Date(parts[0], parts[1] - 1, parts[2])

            var monthNames = [
                "January", "February", "March",
                "April", "May", "June", "July",
                "August", "September", "October",
                "November", "December"
            ];

            var day = newDate.getDate();
            var monthIndex = newDate.getMonth();
            var year = newDate.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
    }

    renderRows() {
        const list = this.props.list || []
        return list.map(todo => (
            <tr key={todo._id} className={todo.howLong > this.props.auth.user.preferredWorkingHourPerDay ? 'danger' : 'success'}>
                <td>{todo.description}</td>
                <td>{todo.howLong}</td>
                <td>{this.formatDate(todo.workedAt)}</td>
                <td>
                    <IconButton style='warning' icon='edit' title='Edit'
                        onClick={() => this.props.toEdit(todo)}></IconButton>
                    <IconButton style='danger' icon='trash-o' title='Remove'
                        onClick={() => this.props.remove(todo)}></IconButton>
                </td>
            </tr>
        ))
    }

    render(){
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>How long</th>
                            <th>Worked at</th>
                            <th className='tableActions'>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
                <div>
                    <IconButton style='success' icon='file-text-o' title='Export list to HTML report'
                            onClick={() => this.exportData()}></IconButton>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({auth: state.auth, list: state.todo.list})
const mapDispatchToProps = dispatch => bindActionCreators({ edit, remove, exportData, toEdit: todo => {
    dispatch(change( "formTodo", "description", todo.description))
    dispatch(change( "formTodo", "howLong", todo.howLong))
    dispatch(change( "formTodo", "workedAt", new Date(todo.workedAt).toISOString().split('T')[0]))
    return {type: 'TODO_EDIT', payload: todo}
 } }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(TodoList)