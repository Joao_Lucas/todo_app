import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field } from 'redux-form'

import Grid from '../template/grid'
import IconButton from '../template/iconButton'
import { search, clear } from './todoActions'
import Input from '../template/input'

class TodoSearch extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.search()
    }

    render() {
        const { search } = this.props
        const { handleSubmit } = this.props
        return (
            <form onSubmit={handleSubmit(v => search(v))} className="form-search">
                <div role='form' className='todoForm'>
                    <Grid cols='12 4 4'>
                        <Field component={Input} type="date" name="begin" id="begin" title='Add a begin date to search' placeholder="Add a begin date to search" />
                    </Grid>
                    <Grid cols='12 4 4'>
                        <Field component={Input} type="date" name="end" id="end" title='Add a end date to search' placeholder="Add a end date to search" />
                    </Grid>
                    <Grid cols='12 4 4'>
                        <label></label>
                        <IconButton style='info' icon='search' type='submit' title='Search'></IconButton>
                        <IconButton style='default' icon='close' onClick={this.props.clear} title='Clear and Search'></IconButton>
                    </Grid>
                </div>
            </form>
        )
    }
}

TodoSearch = reduxForm({ form: 'form-search' })(TodoSearch)
const mapStateToProps = state => ({auth: state.auth})
const mapDispatchToProps = dispatch => bindActionCreators({ search, clear }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(TodoSearch)