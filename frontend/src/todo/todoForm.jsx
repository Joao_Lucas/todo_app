import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, change } from 'redux-form'

import Grid from '../template/grid'
import IconButton from '../template/iconButton'
import { add, edit } from './todoActions'
import Input from '../template/input'

class TodoForm extends Component {
    constructor(props) {
        super(props)
    }

    onSubmit(values) {
        const { add, edit } = this.props
        const  {_id } = this.props.toEdit || ''
        if(_id){
            edit(_id, values)
        } else {
            add(values)    
        }
    }

    render() {
        const { add } = this.props
        const { handleSubmit } = this.props
        return (
            <form onSubmit={handleSubmit(v => this.onSubmit(v))} name='formTodo'>
                <div role='form' className='todoForm'>
                    <Grid cols='12 3 3'>
                        <Field component={Input} type="input" name="description" title='Add a description' placeholder="Add a description" />
                    </Grid>
                    <Grid cols='12 3 3'>
                        <Field component={Input} type="number" name="howLong" title='Add worked hours' placeholder="Add worked hours" />
                    </Grid>
                    <Grid cols='12 3 3'>
                        <Field component={Input} type="date" name="workedAt" title='Add a date' placeholder="Add a date" />
                    </Grid>
                    <Grid cols='12 3 2'>
                        <IconButton style='primary' icon='plus' type='submit' title='Send the new TODO'></IconButton>
                    </Grid>
                </div>
            </form>
        )
    }
}

TodoForm = reduxForm({ form: 'formTodo' })(TodoForm)
const mapStateToProps = state => ({toEdit: state.todo.todoToEdit})
const mapDispatchToProps = dispatch => bindActionCreators({ add, edit }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(TodoForm)
