import consts from '../consts'
const INITIAL_STATE = {description: '', list: [],
                        user: JSON.parse(localStorage.getItem(consts.userKey)),
                        validToken: false
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'DESCRIPTION_CHANGED':
            return { ...state, description: action.payload }
        case 'TODO_SEARCHED':
            return { ...state, list: action.payload }
        case 'TODO_CLEAR':
            return { ...state, description: '' }
        case 'TODO_EDIT':
            return { ...state, todoToEdit: action.payload}
        default:
            return state
    }
}