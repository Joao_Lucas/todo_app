import React, { Component } from 'react'
import Navbar from './navbar'
import If from '../template/if'
import LEVEL from '../levels'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class Menu extends Component {
    render() {
        const { level } = this.props.auth.user || ''
        return (
            <nav className='navbar navbar-inverse bg-inverse'>
            <div className='container'>
                <div className='navbar-header'>
                    <a className='navbar-brand' href='#'>
                        <i className='fa fa-calendar-check-o'></i> TODOApp
                    </a>
                </div>
    
                <div id='navbar' className='navbar-collapse collapse'>
                    <ul className="nav navbar-nav">
                        <If test={level == LEVEL.REGULAR || level == LEVEL.ADMIN}>
                            <li><a href='#/todos'>TO DO List</a></li>
                        </If>
                        <If test={level == LEVEL.MANAGER  || level == LEVEL.ADMIN}>
                            <li><a href='#/users'>Users</a></li>
                        </If>
                        <li><a href='#/about'>About</a></li>
                        <Navbar />
                    </ul>
                </div>
            </div>
        </nav>
        )
    }
}

const mapStateToProps = state => ({auth: state.auth})
export default connect(mapStateToProps, null)(Menu)