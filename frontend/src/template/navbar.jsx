import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {logout} from '../auth/authActions'

class Navbar extends Component {
    constructor(props){
        super(props)
        this.state = {open: false}
    }

    changeOpen(){
        this.setState({open: !this.state.open})
    }

    render(){
        let name = '';
        let email = ''
        if(this.props.user){
            name = this.props.user.name
            email = this.props.user.email
        }
        return(
            <ul className="nav navbar-nav">
                <li onMouseLeave={() => this.changeOpen()}
                    className={`dropdown user user-menu ${this.state.open ? 'open' : ''}`}>
                    <a href="javascript:;" onClick={() => this.changeOpen()}
                        aria-expanded={this.state.open ? 'true' : 'false'}
                        className="dropdown-toggle"
                        data-toggle="dropdown">
                        <span className="hidden-xs">{name}</span>
                    </a>
                    <ul className="dropdown-menu">
                        <li className="user-header">
                            <p>{name}
                            <br />
                            <small>{email}</small></p>
                        </li>
                        <li className="user-footer">
                            <div className="pull-right">
                                <a href="#" onClick={this.props.logout} className="btn btn-default btn-flat">Bye</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        )
    }
}

const mapStateToProps = state => ({user: state.auth.user})
const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Navbar)