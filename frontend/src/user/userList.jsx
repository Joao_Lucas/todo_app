import React,{ Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { change} from 'redux-form'

import IconButton from '../template/iconButton'
import { search, remove } from './userActions'

class UserList extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.search()
    }

    renderRows() {
        const list = this.props.list || []
            return list.map(user => (
                <tr key={user._id}>
                    <td>{user.name}</td>
                    <td>{user.email}</td>
                    <td>{user.level}</td>
                    <td>
                        <IconButton style='warning' icon='edit' title='Edit'
                            onClick={() => this.props.toEdit(user)}></IconButton>
                        <IconButton style='danger' icon='trash-o' title='Remove'
                            onClick={() => this.props.remove(user)}></IconButton>
                    </td>
                </tr>
            ))
    }

    render(){
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Level</th>
                        <th className='tableActions'>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRows()}
                </tbody>
            </table>
        )
    }
}

const mapStateToProps = state => ({auth: state.auth, list: state.user.list})
const mapDispatchToProps = dispatch => bindActionCreators({ search, remove, toEdit: user => {
    dispatch(change( "createUserForm", "name", user.name))
    dispatch(change( "createUserForm", "email", user.email))
    dispatch(change( "createUserForm", "level", user.level))
    return {type: 'USER_EDIT', payload: user}
 }}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(UserList)