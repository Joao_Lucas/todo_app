import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, change} from 'redux-form'

import IconButton from '../template/iconButton'
import Input from '../template/input'
import Grid from '../template/grid'
import { add, edit } from './userActions'
import Select from '../template/select'
import LEVELS from '../levels'

class UserForm extends Component {
    constructor(props) {
        super(props)
    }

    onSubmit(values) {
        const { add, edit } = this.props
        const  {_id } = this.props.toEdit || ''
        if(_id){
            let data = {name: values.name, email: values.email, level: values.level}
            if(values.password && values.confirm_password){
                data = {...data, password: values.password, confirm_password: values.confirm_password}
            }
            edit(_id, data)
        } else {
            add(values)    
        }
    }

    render() {
        const { handleSubmit } = this.props
        const  {_id } = this.props.toEdit || ''
        return (
            <div>
                <form onSubmit={handleSubmit(v => this.onSubmit(v))} name='createUserForm'>
                    <Field component={Input} type="input" name="name" title="Name" placeholder="Name" icon='user'/>
                    <Field component={Input} type="email" name="email" title="E-mail" placeholder="E-mail" icon='envelope' />
                    <Field component={Select} name="level" options={LEVELS} title='Level'/>
                    <Field component={Input} type="password" name="password" title="Password" placeholder="Password" icon='lock' hide={_id ? true : false}/>
                    <Field component={Input} type="password" name="confirm_password" title="Confirm your password" placeholder="Confirm your password" icon='lock' hide={_id ? true : false}/>
                    
                    <Grid cols='12 4 4' additionalCss='col-md-offset-4'>
                        <button type="submit" title='Register'
                            className="btn btn-primary btn-block btn-flat">
                            { _id ? 'Update' : 'Register'}
                        </button>
                    </Grid>
                </form>
            </div>
        )
    }
}

UserForm = reduxForm({ form: 'createUserForm' })(UserForm)
const mapStateToProps = state => ({toEdit: state.user.toEdit})
const mapDispatchToProps = dispatch => bindActionCreators({ add, edit }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(UserForm)