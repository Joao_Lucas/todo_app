import axios from 'axios'
import consts from '../consts'
import { toastr } from 'react-redux-toastr'

const URL = 'http://localhost:3003/api/users'

export const changeDescription = event => ({
    type: 'DESCRIPTION_CHANGED',
    payload: event.target.value
})

export const search = (values = {}) => {
    return (dispatch, getState) => {
        const { auth } = getState()
        let search = `&token=${auth.user.token}`
        const request = axios.get(`${URL}?sort=-workedAt${search}`)
            .then(resp => dispatch({type: 'USER_SEARCHED', payload: resp.data}))
            .catch(e => {
                e.response.data.errors.forEach(
                    error => toastr.error('Ops!', error))
            })
    }
}

export const add = (value) => {
    return (dispatch, getState) => {
        const { auth } = getState()
        axios.post(`${consts.OAPI_URL}/signup`, value)
            .then(resp => {dispatch([{type: 'USER_CREATED', payload: resp.data}])})
            .then(resp => dispatch(search()))
            .catch(e => {
                e.response.data.errors.forEach(
                    error => toastr.error('Ops!', error))
            })
    }
}

export const edit = (id, data, cleanFields) => {
    return (dispatch, getState) => {
        const { auth } = getState()
        axios.put(`${URL}/${id}?token=${auth.user.token}`, data)
            .then(resp => {dispatch([{type: 'USER_CREATED', payload: resp.data}])})
            .then(resp => dispatch(search()))
            .catch(e => {
                e.response.data.errors.forEach(
                    error => toastr.error('Ops!', error))
            })
    }
}

export const remove = (user) => {
    return (dispatch, getState) => {
        const { auth } = getState();
        axios.delete(`${URL}/${user._id}?token=${auth.user.token}`)
            .then(resp => dispatch(search()))
            .catch(e => {
                e.response.data.errors.forEach(
                    error => toastr.error('Ops!', error))
            })
    }
}


export const updateTodoForm = (values) => {
    return (dispatch, change) => {
        change('formTodo', 'description', values.description)
        change('formTodo', 'howLong', values.howLong)
        change('formTodo', 'workedAt', values.workedAt)
        dispatch({type: 'TODO_EDIT', payload: values})
    }

}