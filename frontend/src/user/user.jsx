import React from 'react'

import PageHeader from '../template/pageHeader'
import UserList from './userList'
import Menu from '../template/menu'
import UserForm from './userForm'

export default props => (
    <div>
        <Menu />
        <PageHeader name='Users'></PageHeader>
        <UserForm />
        <UserList />
    </div>
)