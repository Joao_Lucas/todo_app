import consts from '../consts'
const INITIAL_STATE = {list: [],
                       user: JSON.parse(localStorage.getItem(consts.userKey)),
                       validToken: false
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'USER_SEARCHED':
            return { ...state, list: action.payload, toEdit: null }
        case 'USER_CREATED':
            return { ...state}
        case 'USER_CLEAR':
            return { ...state }
        case 'USER_EDIT':
            return { ...state, toEdit: action.payload}
        default:
            return state
    }
}