# TODO List React Application

This project was made in answer to a challenge of writing a simple time management system using React and Node.

## Getting Started

These instructions will get you an up and running copy of the project on your local machine for development and testing purposes.

### Prerequisites

Node and MongoDB

### Installing


First, to execute the application you'll need to clone the repository.

```
git clone https://Joao_Lucas@bitbucket.org/Joao_Lucas/todo_app.git
```

After it, you'll need to download the dependencies to execute the application...

```
cd frontend
npm install
cd ../backend
npm install
```

... and run MongoDB

```
mongod
```

Finally, run the backend and the frontend

```
cd backend
npm run dev

cd frontend
npm run dev
```

The URL application is: http://localhost:8080

## Authors

* **Joao Lucas dos Santos** - *Developer* - [LinkedIn](https://www.linkedin.com/in/joaolucasdossantos/)


