const mongoose = require('mongoose')
mongoose.Promise = global.Promise
module.exports = mongoose.connect('mongodb://localhost/todo', { useMongoClient: true })
// useMongoClient was set true to resolve the alert '(node:7204) DeprecationWarning:'