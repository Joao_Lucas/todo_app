const express = require('express')
const auth = require('./auth')

module.exports = function(server) {

    // Protected API Routes
    const protectedApi = express.Router()
    server.use('/api', protectedApi)
    protectedApi.use(auth)
    
    //TODO API
    const todoService = require('../api/todo/todoService')
    todoService.register(protectedApi, '/todos')
    
    //USER API
    const userService = require('../api/user/userService')
    userService.register(protectedApi, '/users')

    //OPEN API
    const openApi = express.Router()
    server.use('/oapi', openApi)
    const AuthService = require('../api/user/AuthService')
    openApi.post('/login', AuthService.login)
    openApi.post('/signup', AuthService.signup)
    openApi.post('/validateToken', AuthService.validateToken)

    protectedApi.post('/updatePreferredWorkingHourPerDay', AuthService.updatePreferredWorkingHourPerDay)
}