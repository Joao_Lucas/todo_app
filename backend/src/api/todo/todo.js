const restful = require('node-restful')
const mongoose = restful.mongoose

const todoSchema = new mongoose.Schema({
    description: { type: String, required: true },
    done: { type: Boolean, required: true, default: false },
    howLong: { type: Number, required: true },
    workedAt: { type: Date, required: true, default: Date.now },
    userId: {type: String, required: true}
})

module.exports = restful.model('Todo', todoSchema)