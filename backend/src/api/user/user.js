const restful = require('node-restful')
const mongoose = restful.mongoose
const LEVELS = require('./levels')

const userSchema = new mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, min: 6, required: true},
    preferredWorkingHourPerDay: {type: Number, required: false},
    level: {type: String, required: true, default: LEVELS.REGULAR}
})

module.exports = restful.model('User', userSchema)