const LEVELS = {
    REGULAR: 'REGULAR',
    MANAGER: 'MANAGER',
    ADMIN: 'ADMIN'
}

module.exports = LEVELS
