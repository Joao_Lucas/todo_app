const _ = require('lodash')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const User = require('./user')
const env = require('../../.env')

const emailRegex = /\S+@\S+\.\S+/

const sendErrorsFromDB = (res, dbErros) => {
    const errors = []
    _.forIn(dbErrors.errors, error => errors.push(error.message))
    return res.status(400).json({errors})
}

const login = (req, res, next) => {
    const email = req.body.email || ''
    const password = req.body.password || ''

    User.findOne({email}, (err, user) => {
        if(err){
            return sendErrorsFromDB(res, err)
        } else if(user && bcrypt.compareSync(password, user.password)){
            const token = jwt.sign(user, env.authSecret, {
                expiresIn: "1 day"
            })
            const { name, email, id, preferredWorkingHourPerDay, level } = user
            res.json({ name, email, token, id, preferredWorkingHourPerDay, level })
        } else {
            return res.status(400).send({errors: ['User/Password invalid']})
        }
    })
}

const validateToken = (req, res, next) => {
    const token = req.body.token || ''
    jwt.verify(token, env.authSecret, function(err, decoded) {
        return res.status(200).send({valid: !err})
    })
}

const updatePreferredWorkingHourPerDay = (req, res, next) => {
    const id = req.body.id || ''
    const preferredWorkingHourPerDay = req.body.preferredWorkingHourPerDay || ''

    if(id == ''){
        return res.status(400).send({errors: ['User invalid']})
    }

    if(preferredWorkingHourPerDay == ''){
        return res.status(400).send({errors: ['Preferred working hour per day invalid']})
    }

    User.findOne({_id: id}, (err, user) => {
        if(err){
            return sendErrorsFromDB(res, err)
        } else if(user) {
            user.preferredWorkingHourPerDay = preferredWorkingHourPerDay
            
            user.save(err => {
                if(err) {
                    return sendErrorsFromDB(res, err)
                } else {
                    res.json({preferredWorkingHourPerDay: user.preferredWorkingHourPerDay})
                }
            })
        }
    })
}

const signup = (req, res, next) => {
    const name = req.body.name || ''
    const email = req.body.email || ''
    const password = req.body.password || ''
    const confirmPassword = req.body.confirm_password || ''
    const level = req.body.level || ''

    if(email == ''){
        return res.status(400).send({errors: ['E-mail required']})
    } if(!email.match(emailRegex)){
        return res.status(400).send({errors: ['E-mail invalid']})
    }

    if(name == ''){
        return res.status(400).send({errors: ['Name required']})
    }

    if(level == ''){
        return res.status(400).send({errors: ['Level required']})
    }

    const salt = bcrypt.genSaltSync()
    const passwordHash = bcrypt.hashSync(password, salt);
    if(!bcrypt.compareSync(confirmPassword, passwordHash)) {
        return res.status(400).send({errors: ['Password incorrect ']})
    }

    User.findOne({email}, (err, user) => {
        if(err){
            return sendErrorsFromDB(res, err)
        } else if(user) {
            return res.status(400).send({errors: ['User already registered']})
        } else {
            const newUser = new User({name, email, password: passwordHash, level})
            newUser.save(err => {
                if(err) {
                    return sendErrorsFromDB(res, err)
                } else {
                    login(req, res, next)
                }
            })
        }
    })
}

module.exports = {login, signup, validateToken, updatePreferredWorkingHourPerDay}